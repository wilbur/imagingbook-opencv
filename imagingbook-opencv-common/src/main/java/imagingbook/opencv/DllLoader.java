package imagingbook.opencv;


/**
 * This class is used to load a native library (DLL) from an absolute
 * path. It contains a single static method for this purpose.
 * The native library is loaded in the class loader context of the calling class.
 * Note that it is important that a native library is only be loaded once. If loaded
 * again by the same class loader, this has no effect. If loaded by a class
 * loader different to the original class loader, the native library will be loaded
 * again but becomes unaccessible from classes in another class loader context!
 * Thus we make sure that native libraries are always loaded (reloaded) by the *same*
 * stable class loader (e.g., the class loader loading class 'IJ', which is identical
 * to the system class loader. This mechanism is particularly important when reloading
 * ImageJ plugins with 'compile-and-run', since a new class loader is used whenever
 * a plugin is reloaded.
 * 
 * @author W. Burger
 * @version 2016/01/21
 */
public abstract class DllLoader {
	
//	static {
//		System.out.println("static init: loading DllLoader");
//	}
	
	public static boolean loadDllAbs(String path, boolean verbose) {
		if (verbose)
			System.out.println(DllLoader.class.getName() + 
				": loadDllAbs(" + path + "). Class loader = " +
				DllLoader.class.getClassLoader());
		boolean result = false;
		try {
			System.load(path);
			result = true;
			if (verbose)
				System.out.println(DllLoader.class.getName() + ": loaded DLL from " + path);
		} catch (UnsatisfiedLinkError ule) {
			if (verbose) {
				System.out.println(DllLoader.class.getName() + ": failed to load DLL from " + path);
				ule.printStackTrace(System.out);
			}
		}
		return result;
	}
	
}
