package imagingbook.opencv;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import ij.IJ;

public class FileUtils {
	
	protected static void listUrls(URLClassLoader ucl) {
		System.out.println("~~~Listing URLs of " + ucl);
		for (URL url : ucl.getURLs()) {
			System.out.println("        url = " + url);
		}
	}
	
	/**
	 * Add an URL to an URLCLassLoader using reflection.
	 * @param ucl
	 * @param url
	 * @return
	 */
	protected static boolean addUrl(URLClassLoader ucl, URL url) {
		Method m = null;
		try {
			m = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
			m.setAccessible(true);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Object o = null;
		try {
			o = m.invoke(ucl, url);
		} catch (Exception ignored) {}
		
		return (o != null);
	}
	
	protected static URL urlFromFile(File file) {
		URL url = null;
		try {
			url = file.toURI().toURL();
		} catch (MalformedURLException e2) {
			e2.printStackTrace();
		}
		return url;
	}
	
	protected static void listNativeLibraries() {
		Field f = null;
		try {
			f = ClassLoader.class.getDeclaredField("loadedLibraryNames");
		} catch (Exception e) {
			e.printStackTrace();
		}
		f.setAccessible(true);
		try {
			@SuppressWarnings("unchecked")
			Vector<String> dllNames = (Vector<String>) f.get(null);
			for (String dll : dllNames) {
				IJ.log("  > " + dll);
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Searches the specified directory for a file whose name starts and ends with
	 * a specific string.
	 * @param directory	the directory to be searched (non-recursively)
	 * @param startsWith the substring required at the beginning of the file name
	 * @param extension the required file extension, e.g. ".jar"
	 * @return
	 */
	protected static String findFile(String directory, String startsWith, String extension) {
		File dir = new File(directory);
		if (!dir.isDirectory()) {
			throw new Error("not a directory: " + directory);
		}
		File[] files = dir.listFiles();
		String filename = null;
		for (File f : files) {
			String name = f.getName();
			//System.out.println("    file: "  + name);
	        if (name.startsWith(startsWith) && name.endsWith(extension)) {
	        	filename = name;
	        	break;
	        }
	    }
		if (filename == null) {
			throw new Error("[findFile] found no such file: " + startsWith + "XXX" + extension);
		}
		return filename;
	}
	
	public static void listLoadedLibraries() {
		System.out.println("Listing LOADED LIBRARIES:");
		Field f = null;
		try {
			f = ClassLoader.class.getDeclaredField("loadedLibraryNames");
		} catch (Exception e) {
			e.printStackTrace();
		}
		f.setAccessible(true);
		try {
			@SuppressWarnings("unchecked")
			Vector<String> dllNames = (Vector<String>) f.get(null);
			for (String dll : dllNames) {
				System.out.println("  > " + dll);
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	public static void listLibraryPath() {
		System.out.println("Listing LIBRARY PATH:");
		String separator = System.getProperty("path.separator");
		String path = System.getProperty("java.library.path");
		String[] entries = path.split(separator);
		for (String s : entries) {
			System.out.println("  > " + s);
		}
	}
	
	public static void listClassPath() {
		System.out.println("Listing CLASS PATH:");
		String separator = System.getProperty("path.separator");
		String path = System.getProperty("java.class.path");
		String[] entries = path.split(separator);
		for (String s : entries) {
			System.out.println("  > " + s);
		}
	}
	
	// -------------------------------------------------------------
	
	// adapted from http://stackoverflow.com/questions/4748673/how-can-i-check-the-bitness-of-my-os-using-java-j2se-not-os-arch/5940770
	protected static int getOsBits() {
		String arch = System.getenv("PROCESSOR_ARCHITECTURE");
		String wow64Arch = System.getenv("PROCESSOR_ARCHITEW6432");
		return arch.endsWith("64") || wow64Arch != null && wow64Arch.endsWith("64")  ? 64 : 32;
	}

}
