https://imagingbook@bitbucket.org/imagingbook/opencv-ij.git


See for specific Maven/pom setup:
https://github.com/JavaOpenCVBook/code/blob/master/chapter1/maven-sample/my-opencv-app/pom.xml
http://code.opencv.org/issues/3097
https://github.com/Itseez/opencv/issues/4588
https://github.com/JavaOpenCVBook/code/wiki/Tutorial-1----Java-OpenCV-5-minute-setup

NOTE: this repo is INCOMPLETE (native lib and output files missing)!




-----------------------------------------------------------------------------------------
ImageJ - OpenCV Bridge (imagingbook.opencv)
-----------------------------------------------------------------------------------------


This project provides a bridge between ImageJ and OpenCV's Java build (currently 
to be used under Windows only).

Since recent versions of OpenCV come with a dedicated Java build, working with OpenCV in Java is 
usually a simple issue. This is different if OpenCV is used by ImageJ plugins that are launched
by 'Compile-and-run'. In this case, the plugin is loaded in a new Java class loader which conflicts
with the requirement that native libraries (as needed to run OpenCV) must be loaded only once in 
the lifetime of a Java runtime or -- if necessary -- must be reloaded with the same class loader.

The solution implemented in this package is not trivial. It uses the distinct class loader of 
ImageJ's 'IJ' class (which is identical to Java's system class loader) to load OpenCV's native
library (DLL) as well as the associated JAR file. To make this possible, the search path of 
the IJ class loader is extended (using reflection).

The installation requires the following files to be in place ('ImageJ' denotes ImageJ's
root directory):

---------------------------------------------------------------------
ImageJ/jars/
	opencv-ij.jar  (JAR file containing THIS package)
	opencv/
		opencv-310.jar
		x64/opencv_java310.dll
		x86/opencv_java310.dll
-----------------------------------------------------------------------------------------
ImageJ/plugins/opencv_tests/
	plugins for testing the installation
-----------------------------------------------------------------------------------------

For convenience, all necessary files in ImageJ/jars/ are packed into a single ZIP file to be expanded
inside ImageJ/jars/.
 
The contents of ImageJ/opencv are copies from the official OpenCV build for Windows
(\OpenCV\3.1\win\extract\opencv\build\java). The correct .dll version is selected automatically
at load time, any unnecessary DLL (they are big) can be removed.
OpenCV versions are not hard-coded, i.e., never versions of the .dll and .jar files should
be found automatically (assuming that naming conventions remain the same).

Also see OpenCV installation instructions here:
  http://docs.opencv.org/doc/tutorials/introduction/java_eclipse/java_eclipse.html#java-eclipse
  http://docs.opencv.org/3.0-beta/doc/tutorials/introduction/desktop_java/java_dev_intro.html

OpenCV-JavaDoc:
  http://docs.opencv.org/java/3.1.0/
  
Note: This package does not depend on the 'imagingbook' library.


W. Burger
www.imagingbook.com


